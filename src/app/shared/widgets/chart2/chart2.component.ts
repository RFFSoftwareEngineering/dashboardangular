import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-chart2',
  templateUrl: './chart2.component.html',
  styleUrls: ['./chart2.component.css'],
})
export class Chart2Component implements OnInit {
  chartOptions: {};

  Highcharts = Highcharts;

  constructor() {}

  ngOnInit() {
    this.chartOptions = {
      chart: {
        type: 'pie',
        options3d: {
          enabled: true,
          alpha: 45,
        },
      },
      title: {
        text: 'Top Purchased Products in 2022',
      },
      subtitle: {
        text: '3D donut in Highcharts',
      },
      plotOptions: {
        pie: {
          innerSize: 100,
          depth: 45,
        },
      },
      series: [
        {
          name: 'Delivered amount',
          data: [
            ['SD16', 8],
            ['SL2498', 3],
            ['SD32', 1],
            ['SD34', 6],
            ['SL2596', 8],
            ['SD49', 4],
            ['SL2699', 4],
            ['SD36', 1],
            ['SD40', 1],
          ],
        },
      ],
    };
  }
}
