import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  Highcharts = Highcharts;

  chartOptions = {};

  constructor() {}

  ngOnInit() {
    this.chartOptions = {
      chart: {
        type: 'areaspline',
      },
      title: {
        text: null,
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
      },
      xAxis: {
        categories: [
          'Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday',
        ],
        plotBands: [
          {
            // visualize the weekend
            from: 4.5,
            to: 6.5,
            color: 'rgba(68, 170, 213, .2)',
          },
        ],
      },
      yAxis: {
        title: {
          text: null,
        },
      },
      tooltip: {
        shared: true,
        valueSuffix: ' units',
      },
      credits: {
        enabled: false,
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.5,
        },
      },
      series: [
        {
          name: 'SD16 light pink',
          data: [3, 4, 3, 5, 4, 10, 12],
        },
        {
          name: 'SD16 suntan',
          data: [1, 3, 4, 3, 3, 5, 4],
        },
      ],
    };
  }
}
